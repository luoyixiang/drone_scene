import rospy

from hector_uav_msgs.srv import EnableMotors
from hector_uav_msgs.msg import Altimeter
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

cmd_pub = rospy.Publisher("cmd_vel", Twist, queue_size=1)


z = 0

def callback_takeoff_2(msg):
    global z
    z = msg.pose.pose.position.z

    u_z = 2.0 * (10.0 - z)

    cmd_msg = Twist()
    cmd_msg.linear.z = u_z

    cmd_pub.publish(cmd_msg)


def complex_controller():
    global z 
    u_z = k * (z_des - z)
    u_y = ky * (y_des - y)
    u_w = kw * (w_des - w)


def main():
    rospy.init_node("test_control")

    rospy.wait_for_service("/enable_motors")
    foo2call = rospy.ServiceProxy("/enable_motors", EnableMotors)
    if foo2call(True):
        print("Motors started!")
    
    rospy.Subscriber("/ground_truth/state", Odometry, callback_takeoff_2)

    rospy.spin()
if __name__=="__main__":
    main()